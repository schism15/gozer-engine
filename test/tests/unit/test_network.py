import unittest
import os

from test.fixtures.mockserver import MockGopherServer
from src.gozer_engine.network import NetworkClient

def init_server():
    server = MockGopherServer()

class NetworkClientTest(unittest.TestCase):
    def setUp(self):
        self.server_pid = None
        pid = os.fork()
        if not pid:
            return init_server()
        self.server_pid = pid

    def test_request_url_without_trailing_slash(self):
        # Not implemented
        pass

    def tearDown(self):
        if not self.server_pid:
            return
        import signal
        os.kill(self.server_pid, signal.SIGINT)

    # IMP: Initite the components, then the browser engine

    # IMP: Start the browser engine with an initial URI

    # IMP: Check the page source for the correct reponse