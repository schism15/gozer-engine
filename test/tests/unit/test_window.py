import unittest
import curses
from curses import wrapper
import time

from test.fixtures.mock_dom import mock_dom

from src.gozer_engine.window import Canvas

# TODO: 'i' type lines should have active value of None
class BrowserWindowTest(unittest.TestCase):
    def test_set_active_link_state(self):
        canvas = Canvas()
        canvas._dom = mock_dom
        canvas._refresh_link_map()
        assert len(canvas._link_map) == 31
        active_links = [link for link in canvas._link_map if link['active'] is True]
        assert len(active_links) == 0
        
        # Toggle active link at position 1
        canvas._set_link_active_state(1, active=True)
        assert len([x for x in canvas._link_map if x['active'] is True]) == 1
        assert canvas._link_map[1]['active'] is True
        active_links = [link for link in canvas._link_map if link['active'] is True]
        assert len(active_links) == 1

        # Toggle active link from position 1 to 2
        current_pos, current_active = canvas._get_active_link()
        target_pos, target_active = (current_pos + 1, canvas._link_map[current_pos + 1])
        canvas._set_link_active_state(current_pos, active=False)
        canvas._set_link_active_state(target_pos, active=True)
        assert len([x for x in canvas._link_map if x['active'] is True]) == 1
        assert canvas._link_map[current_pos]['active'] is False
        assert canvas._link_map[target_pos]['active'] is True
        canvas._refresh_link_map()
        active_links = [link for link in canvas._link_map if link['active'] is True]
        assert len(active_links) == 1

        # Toggle active link from position 2 to 3
        current_pos, current_active = canvas._get_active_link()
        target_pos, target_active = (current_pos + 1, canvas._link_map[current_pos + 1])
        canvas._set_link_active_state(current_pos, active=False)
        canvas._refresh_link_map()
        canvas._set_link_active_state(target_pos, active=True)
        assert len([x for x in canvas._link_map if x['active'] is True]) == 1
        assert canvas._link_map[target_pos]['active'] is True
        active_links = [link for link in canvas._link_map if link['active'] is True]
        assert len(active_links) == 1