import unittest
import os

from test.fixtures.mockserver import MockGopherServer
from src.gozer_engine.gozer import Gozer

def init_server():
    server = MockGopherServer()

class BrowserStartTestNoUri(unittest.TestCase):
    def test_browser_start_no_uri(self):
        '''Test that the browser starts
        and shows the splash page'''

        session = Gozer()
        session.start()
        page_contents = session.get_page_contents()
        assert 'Welcome to Gozer, a terminal based Gopher browser.' in page_contents

class BrowserStartTestWithUri(unittest.TestCase):
    def setUp(self):
        self.server_pid = None
        pid = os.fork()
        if not pid:
            return init_server()
        self.server_pid = pid

    def test_browser_start_with_uri(self):
        '''Test that the browser starts
        and shows the splash page'''
        uri = '127.0.0.1'
        session = Gozer()
        session.start(uri=uri)
        page_contents = session.get_page_contents()
        assert 'Welcome to **Gopherpedia**, the gopher interface to Wikipedia.' in page_contents

    def tearDown(self):
        if not self.server_pid:
            return
        import signal
        os.kill(self.server_pid, signal.SIGINT)

class TestBrowserStartThenSubmitUri(unittest.TestCase):
    def setUp(self):
        self.server_pid = None
        pid = os.fork()
        if not pid:
            return init_server()
        self.server_pid = pid

    def test_browser_start_then_submit_uri(self):
        '''Test that the browser starts
        and shows the splash page'''
        uri = '127.0.0.1'

        session = Gozer()
        session.start()
        page_contents = session.get_page_contents()
        assert 'Welcome to Gozer, a terminal based Gopher browser.' in page_contents

        session.send_request(uri)
        page_contents = session.get_page_contents()
        assert 'Welcome to **Gopherpedia**, the gopher interface to Wikipedia.' in page_contents


    def tearDown(self):
        if not self.server_pid:
            return
        import signal
        os.kill(self.server_pid, signal.SIGINT)